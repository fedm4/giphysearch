App.service('GiphyService', function($timeout, $q, $http, __env){

  var getTop = function () {
            //Load trending and resolve as "topImages"
            var url = __env.apiUrl + "trending?api_key=" + __env.apiKey;
            //Define promise
            var deferred = $q.defer();
            //Call GET and resolve if OK, reject if error
            $http.get(url).then(function(response){
                  deferred.resolve({ topImages: response.data.data });
            }, function(msg, code) {
              //TODO: Handle errors
              deferred.reject(msg);
              console.log(msg);
              console.log(code);
            });
            return deferred.promise;
  };

  var getSearch = function (query, offset) {
            //Load search and return as "images"
            var url = __env.apiUrl + "search?q=" + query + "&offset=" + offset + "&api_key="+__env.apiKey;
            //Define promise
            var deferred = $q.defer();
            //Call GET and resolve if OK, reject if error
            $http.get(url).then(function(response){
                  deferred.resolve({ images: response.data.data });
            }, function(msg, code) {
              //TODO: Handle errors
              deferred.reject(msg);
              console.log(msg);
              console.log(code);
            });
            return deferred.promise;
  };

  return {
    getTop: getTop,
    getSearch: getSearch,
  }
});
