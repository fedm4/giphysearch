App.service('SearchService', function(){
  // query property and it's getter/setter
  var query = '',
      setSearch = function(input){
        query = input;
      },
      getSearch = function(input){
        return query;
      };

  return {
      setSearch: setSearch,
      getSearch: getSearch,
  }
});
