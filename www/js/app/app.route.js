angular.module('app')
       .config(function ($routeProvider) {

         //Routes
         $routeProvider
         .when("/", {
           templateUrl: "js/app/templates/index.html",
           controller: "IndexController",
         })
         .when("/search", {
           templateUrl: "js/app/templates/search.html",
           controller: "SearchController",
         });
});
