App.controller('SearchController', function($scope, $location, $window, GiphyService, SearchService){
        //Load the search through the SearchService shared with HeaderController
        $scope.search = SearchService.getSearch();
        $scope.images = [];
        $scope.query = '';
        $scope.offset = 0;

        //Prepare Query String
        $scope.makeQuery = function(){
          //Conver each word (Separated by space) into element in array, and loop for each
          angular.forEach($scope.search.split(' '), function(value, key){
            $scope.query += value + '+';
          });
          //Remove Last Character
          $scope.query = $scope.query.slice(0, -1);
        };

        $scope.getSearch = function(){
          GiphyService.getSearch($scope.query, $scope.offset)
                  .then(function(payload){
                      //angular.merge($scope.images, $scope.images, payload.images);
                      angular.forEach(payload.images, function(value){
                        $scope.images.push(value);
                      });
                      $scope.offset += 25;
                    },
                    function(errorPayload){
                      console.log('Error Payload');
                      console.log(errorPayLoad);
                      //TODO: Handle Error
                    }
                  );
        };


        //Functions to get users scroll to bottom
        //below taken from http://www.howtocreate.co.uk/tutorials/javascript/browserwindow
        function getScrollXY() {
            var scrOfX = 0, scrOfY = 0;
            if( typeof( window.pageYOffset ) == 'number' ) {
                //Netscape compliant
                scrOfY = window.pageYOffset;
                scrOfX = window.pageXOffset;
            } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
                //DOM compliant
                scrOfY = document.body.scrollTop;
                scrOfX = document.body.scrollLeft;
            } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
                //IE6 standards compliant mode
                scrOfY = document.documentElement.scrollTop;
                scrOfX = document.documentElement.scrollLeft;
            }
            return [ scrOfX, scrOfY ];
        }

        //taken from http://james.padolsey.com/javascript/get-document-height-cross-browser/
        function getDocHeight() {
            var D = document;
            return Math.max(
                D.body.scrollHeight, D.documentElement.scrollHeight,
                D.body.offsetHeight, D.documentElement.offsetHeight,
                D.body.clientHeight, D.documentElement.clientHeight
            );
        }
        //If user scrolls to bottom
        angular.element($window).bind("scroll", function() {
          if (getDocHeight() == getScrollXY()[1] + window.innerHeight) {
            $scope.getSearch();
          }
        });


        //Run functions on load view
        $scope.makeQuery();
        $scope.getSearch();



});
