App.controller('IndexController', function($scope, GiphyService){
        $scope.topImages = [];

        //On Index load trending
        GiphyService.getTop()
                    .then(function(payload){
                        $scope.topImages = payload.topImages;
                      },
                      function(errorPayload){
                        console.log('Error Payload');
                        console.log(errorPayLoad);
                        //TODO: Handle Error
                      }
        );
});
