App.controller('HeaderController', function($scope, $location, SearchService){
        $scope.search = '';

        $scope.go = function ( path ) {
          $location.url( path );
        };

        $scope.doSearch = function(){
          //Set the query through the SearchService shared with SearchController
          SearchService.setSearch($scope.search);
          //Change URL to reload view with new data and verbose querystring
          $location.url('/search?query=' + $scope.search);
        };
});
