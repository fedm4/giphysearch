var env = {};

if(window){
  Object.assign(env, window.__env);
}

var App = angular.module('app',['ngRoute'])
                 .constant('__env', env);
