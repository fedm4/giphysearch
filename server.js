var express = require('express'),
    app = express();

/* Set the view engine - Jade */
app.set('views', './views')
   .set('view engine', 'jade')
   .use(express.static('www'));

/* Handle base route */
app.get('/', function (req, res) {
  res.render('index',{
                      title: 'Giphy Search',
                      message: 'Testing 123, AEIOU'
                    }
            );
});

app.listen(5000, function () {
  console.log('Go to http://localhost:5000 to run the application.');
});
