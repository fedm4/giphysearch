/*jshint node:true, strict: false*/
var gulp = require('gulp'),
	sass = require('gulp-sass'),
	minifyCss = require('gulp-clean-css'),
	autoprefixerOptions = {
      browsers: ['last 2 versions', '> 5%', 'Firefox ESR', 'ie 8-11']
	},
	input = './sass/**/*.scss',
	output = './www/css/',
	sassOptions = {};

gulp.task('sass', function () {
  'use strict';
  return gulp
    .src(input)
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(minifyCss({zindex:false, autoprefixer:autoprefixerOptions}))
    .pipe(gulp.dest(output));
});

gulp.task('watch', function() {
  'use strict';
  return gulp
    .watch(input, ['sass'])
    .on('change', function(event) {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

gulp.task('default', ['sass', 'watch']);
